FROM alpine:latest

ENV LANG=en_US.UTF-8 \
    LANGUAGE=en_US:en \
    LC_ALL=en_US.UTF-8

RUN set -ex && \
    apk update && \
    apk upgrade && \
    apk --no-cache add git openssh-client curl ca-certificates python3 python3-dev linux-headers gcc musl-dev && \
    pip3 install --upgrade --no-cache-dir pip setuptools python-openstackclient && \
    apk del python3-dev linux-headers gcc musl-dev && \
    rm -rf -- /var/cache/apk/* /var/lib/apk/* /etc/apk/cache/*